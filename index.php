<?php

require_once('ape.php');
require_once('frog.php');

print_r("Release 0");
$sheep = new animal("Shaun");

echo "<br><br>Animal name : ". $sheep -> name;
echo "<br>Number of legs : ". $sheep -> legs;
echo "<br>Is animal cold blooded : ". $sheep -> blood;

print_r("<br><br><br>Release 1");

$sungokong = new ape("kera sakti");
echo "<br><br>Animal name : ". $sungokong -> name;
echo "<br>Number of legs : ". $sungokong -> legs;
echo "<br>Is animal cold blooded : ". $sungokong -> blood;
echo "<br>Yell  : ";
$sungokong->yell();

$kodok = new frog("buduk");
echo "<br><br>Animal name : ". $kodok -> name;
echo "<br>Number of legs : ". $kodok -> legs;
echo "<br>Is animal cold blooded : ". $kodok -> blood;
echo "<br>Jump  : ";
$kodok->jump();